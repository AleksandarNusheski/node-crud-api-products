const express = require('express'),
	app = express(),
	bodyParser = require('body-parser'),
	mongoose = require('mongoose'),
	port = process.env.PORT || 8080;

// bodyParser extracts the entire body portion of an incoming request stream and exposes it on req.body
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// set the routes
app.use(require('./app/routes'));
// connect to database
mongoose.connect('mongodb://test:test@ds135532.mlab.com:35532/products');

// start the server
app.listen(port, function(){
	console.log('App listening on port: '+port);
});
