const Product = require('./model');

module.exports = {
	getAllProducts: getAllProducts,
	createProduct: createProduct,
	getOneProduct: getOneProduct,
	deleteProduct: deleteProduct,
	editProduct: editProduct
}

function getAllProducts(req, res){
	Product.find(function(err, products){
		if(err) res.send(err);
		else res.json(products);
	});
}

function createProduct(req, res){
	var newProduct = new Product();
	newProduct.name = req.body.name;
	newProduct.price = req.body.price;
	newProduct.available = req.body.available;
	newProduct.dateCreated = new Date().toString();
	newProduct.save(function(err){
		if(err) res.send(err);
		else res.json({message: 'New product successfully created'});
	});
}

function getOneProduct(req, res){
	Product.findById(req.params.id, function(err, product){
		if(err) res.send(err);
		else res.json(product);
	});
}

function deleteProduct(req, res){
	Product.remove({_id: req.params.id}, function(err){
		if(err) res.send(err);
		else res.json({message: 'Product successfully deleted'});
	});
}

function editProduct(req, res){
	Product.findById(req.params.id, function(err, product){
		if(err) res.send(err);
		else{
			product.name = req.body.name;
			product.price = req.body.price;
			product.available = req.body.available;
			product.save(function(err){
				if(err) res.send(err);
				res.json({message: 'Product successfully edited'});
			});
		}
	});
}
