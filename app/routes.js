// create a new express router
const express = require('express'),
	router = express.Router(),
	controller = require('./controller');

// define routes
router.get('/products', controller.getAllProducts);
router.post('/product', controller.createProduct);
router.get('/product/:id', controller.getOneProduct);
router.delete('/product/:id', controller.deleteProduct);
router.put('/product/:id', controller.editProduct);

// export router
module.exports = router;
