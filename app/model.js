const mongoose = require('mongoose'),
	Schema = mongoose.Schema;

// create a schema
var ProductSchema = new Schema({
	name: String,
	price: Number,
	available: Boolean,
	dateCreated: String
});

// create and export the model
module.exports = mongoose.model('Product', ProductSchema);
