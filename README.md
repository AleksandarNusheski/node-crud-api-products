### Naloga: ###

Prosil bi te, da napišeš majhen CRUD API z uporabo Express za routing in Mongoose za interakcijo z MongoDB bazo. Spodaj je seznam routov, ki jih naj API omogoča.

* [GET] /products
* [POST] /product
* [GET] /product/:id
* [DELETE] /product/:id
* [PUT] /product/:id

Na [GET] /products bi želel videt response s spodnjo shemo. Podatki so simbolični.

![Screenshot1](pictures/getProducts1.jpg)

### Rešitev: ###

![Screenshot2](pictures/getProducts2.jpg)
